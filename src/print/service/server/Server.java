/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package print.service.server;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Print.PrintInfoContainer;
import java.io.*;
import java.net.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors.*;
import java.util.stream.Stream;

public class Server {
   static ArrayList<Socket> clientOutputStreams;
   static ArrayList<PrintInfoContainer> users;
   static ArrayList<ObjectOutputStream> sosockets;
   static ArrayList<String> names;
   public static class ClientHandler implements Runnable
   {
       Socket sock;
       ObjectInputStream ois;
       ObjectOutputStream oos;
       
       public  ClientHandler(Socket clientSocket) throws ClassNotFoundException 
       {
            try 
            {
                sock = clientSocket;
                clientOutputStreams.add(sock);
                System.out.println(sock.getInetAddress());
                ois = new ObjectInputStream(new BufferedInputStream(sock.getInputStream()));
                oos = new ObjectOutputStream(sock.getOutputStream());
                Object obj = ois.readObject();
                String tmpName = (String)obj;
                oos.writeBoolean(!names.contains(tmpName));
                oos.flush();
                
                obj = ois.readObject();
                ArrayList getprinter = (ArrayList)obj;
                if(!getprinter.isEmpty())
                    users.addAll(getprinter);
               
                sosockets.add(oos);
                oos.writeObject(users);
                oos.flush();
                names.add(tmpName);
            }
            catch (IOException ex) 
            {
                System.out.println(ex.getLocalizedMessage());
            }

       }
       
      
       @Override
       public void run() 
       {
            String message;
            String[] data;
            boolean isDisconnected = false;
            try 
            {
                while ((message = (String)ois.readObject()) != null) 
                {
                    data = message.split(":");
                    switch(data[2]){
                        case "Connect": System.out.println((data[0] + ":has connected"));
                            break;
                        case "Disconnect":
                            System.out.println((data[0] + ":has disconnected."));
                            int id = clientOutputStreams.indexOf(sock);
                            sosockets.remove(id);
                            clientOutputStreams.remove(sock);
                            final String hostAddress = sock.getInetAddress().getHostAddress();
                            users.removeIf(p-> p.getIp().equals(hostAddress));
                            isDisconnected = true;
                            break;
                        case "TDisconnect":
                            break;
                        case "getPrinter":
                            oos.writeObject("com:command:setPrinter");
                            oos.flush();
                            ObjectOutputStream set = new ObjectOutputStream(sock.getOutputStream());
                            set.writeObject(users);
                            set.flush();
                            System.out.println("Список принтеров отправлен");
                            break;
                        case "OpenServer":
                            int j = 0;
                            for(int i=0; i<clientOutputStreams.size(); i++)
                            {
                                String tmp = clientOutputStreams.get(i).getInetAddress().getHostAddress();
                                if(tmp.equals(data[1]))
                                {
                                    j=i;
                                    break;
                                }
                            }
                            ObjectOutputStream obj = (ObjectOutputStream)sosockets.get(j);
                            obj.writeObject("com:command:OpenServer");
                            oos.writeObject("com:command:OpenClient");
                        default:
                            System.out.println("No Conditions were met. \n");
                            break;
                    }
                } 
             } 
             catch (Exception ex) 
             {
                System.out.println("Lost a connection. \n");
                if(!isDisconnected){
                int id = clientOutputStreams.indexOf(sock);
                sosockets.remove(id);
                clientOutputStreams.remove(sock);
                final String hostAddress = sock.getInetAddress().getHostAddress();
                users.removeIf(p-> p.getIp().equals(hostAddress));
                } 
            }
        }    
    }
    public static class ServerStart implements Runnable
    {
        @Override
        public void run() 
        {
            clientOutputStreams = new ArrayList();
            users = new ArrayList();  
            sosockets = new ArrayList();
            names = new ArrayList();
            try 
            {
                ServerSocket serverSock = new ServerSocket(2222);
                System.out.println(InetAddress.getLocalHost().getHostAddress());
                while (true) 
                {
                    Socket clientSock = serverSock.accept();
                    Thread listener = new Thread(new ClientHandler(clientSock));
                    listener.start();
                    System.out.println("Got a connection. \n");
                }
            }
            catch (Exception ex)
            {
                System.out.println("Error making a connection. \n");
                System.err.println(ex.getLocalizedMessage());
            }
        }
    } 
    public static void main(String[] args) throws ClassNotFoundException, SQLException 
    {
        Server.ServerStart ins =  new Server.ServerStart();
        ins.run();
    }

}
